﻿<?php

	/* -----------
	   PREFERENCES
	   ----------- */
	$your_email = "craig@forsythdesign.ca";

	$ok_message = "<h4>Thanks for your interest in Forstyh Design, I will get back to you shortly about your inquiry.</h4>";

	$fail_message = "Oops, there's something wrong with the system! Please email us directly: <a href=\"mailto:".$your_email."\">".$your_email."</a>";

	/* -----------
	   APP
	   ----------- */

	if (isset($_POST['submit'])) {
		$msg = $_POST['name'] . " <" . $_POST['email'] . "> " . "\n\nReferral: " . $_POST['refer'] . "\n\nMessage:\n" . $_POST['text'] ;
			mail($your_email, 'FROM FORSYTHDESIGN.CA', $msg);
		echo $ok_message;
	} else {
		echo $fail_message;
	}
?>