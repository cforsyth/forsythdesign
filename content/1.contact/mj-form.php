﻿<?php
	/* -----------
	   PREFERENCES
	   ----------- */
	$base = "http://www.forsythdesign.ca";
?>
<div id="mj-contact-form" >
	<div id="mj-response" style="display:none;">&nbsp;</div>
	 <form class="contact-form">
	 	<fieldset id="mj-contact">

			<label for="name">Name: <em>*</em></label>	
			<input class="mj-req" type="text" name="name" id="mj-name" value="">

			<label for="email">Email: <em>*</em></label>
			<input type="email" required name="email" class="mj-req" id="mj-email" value="">


			<label for="refer">Type of Project:<em>*</em></label>

			<select name="refer" size="1" id="mj-refer" class="mj-req">
				<option value="">-- Choose --</option>
				<option value="Front-end Developer">Front-end Development</option>
				<option value="Web Design">Web Design</option>
		    </select>

		</fieldset>
		<fieldset id="mj-message">
			<label for="text">Your message: </label>
			<textarea name="text" id="mj-text"></textarea>
		</fieldset>
			<input type="submit" id="mj-submit" name="submit" />
			<p class="subtext"><em>*</em> = required <small>Javascript must be enabled!</small></p>

	</form></div>

<script type="text/javascript">
	$(document).ready(function() {

		$("form").bind("keypress", function(e) {
             if (e.keyCode == 13) {
                 e.preventDefault();
            }


         });

		$("#mj-submit").click(function(e) {
			e.preventDefault();
			var checked = true;
			$("input.mj-req, select.mj-req").removeClass("mj-recheck");
			$("input.mj-req, select.mj-req").each(function(index, value) {
				if ($(this).val() == null || $(this).val() == "") {
					checked = false;
					$(this).addClass("mj-recheck");
					$("#mj-response").text("Please check your input.").css('display','block');
				}
			});

			if (checked) {

				$("#mj-response").text("Please wait...").css('display','block');
				$("#mj-response").load("<?=$base?>/php/mj-contact.php", {
					submit: true,
					name: $("#mj-name").val(),
					email: $("#mj-email").val(),
					text: $("#mj-text").val(),
					refer: $("#mj-refer").val(),
				}).css('display','block').fadeOut(5000);

				$("#mj-contact-form form").remove();
				
				setTimeout(function(){
					window.location.replace("http://www.forsythdesign.ca").fadeOut();}, 3500);
			

			} else {

			}
		});
	});
</script>